//
//  AppDelegate.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import UIKit
import SwiftUI

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    // MARK: - Propertirs
    var window: UIWindow?
    private var contactsCoordinator: ContactsCoordinator?
    
    // MARK: - Public methods
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        contactsCoordinator = ContactsCoordinator()
        contactsCoordinator?.start()
        self.window?.rootViewController = contactsCoordinator?.navigationController
        self.window?.makeKeyAndVisible()
        
        return true
    }
}
