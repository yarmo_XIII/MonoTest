//
//  SelectedContactsView.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import SwiftUI

// MARK: - SelectedContactsView
struct SelectedContactsView: View {
    @ObservedObject var viewModel: SelectedContactsViewModel
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        topBar()
        List(viewModel.sortedContacts) { contact in
            ContactRow(
                contact: contact,
                isSelected: true,
                isCheckmarkHidden: true
            )
        }
    }
    
    func topBar() -> some View {
        HStack {
            Button {
                dismiss()
            } label: {
                Image(systemName: Constants.arrowIconName)
                    .foregroundColor(.blue)
            }
            Text(Constants.topBarText)
                .lineLimit(1)
                .font(.headline)
                .frame(maxWidth: .infinity, alignment: .center)
        }
        .padding(.vertical)
        .padding(.horizontal)
    }
}

// MARK: - Constants
private extension SelectedContactsView {
    enum Constants {
        static let arrowIconName = "arrow.backward"
        static let topBarText = "Selected contacts"
    }
}
