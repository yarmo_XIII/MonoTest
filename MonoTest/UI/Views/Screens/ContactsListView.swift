//
//  ContactsListView.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import SwiftUI

// MARK: - ContactsListView
struct ContactsListView: View {
    var coordinator: ContactsCoordinator
    @ObservedObject var viewModel: ContactsListViewModel
    
    var body: some View {
        VStack {
            topBar()
            if !viewModel.isContactsEmpty {
                SearchBarView(text: $viewModel.searchText)
            }
            contentView
            continueButton
        }
        .hideKeyboardOnTap()
    }
}

// MARK: - Views
private extension ContactsListView {
    func topBar() -> some View {
        Text(Constants.contactsText)
            .lineLimit(1)
            .font(.headline)
            .frame(maxWidth: .infinity, alignment: .center)
            .padding(.vertical)
    }
    
    var continueButton: some View {
        Button(action: {
            coordinator.showSelectedContacts(selectedContacts: viewModel.selectedContacts)
        }, label: {
            Text(Constants.continueText)
                .fontWeight(.bold)
                .foregroundColor(.white)
                .frame(height: 46)
                .frame(maxWidth: .infinity, alignment: .center)
                .background(
                    viewModel.selectedContacts.isEmpty ? LinearGradient.disabledButtonGradient : LinearGradient.enabledButtonGradient
                )
                .clipShape(.capsule)
                .disabled(!viewModel.isContinueButtonEnabled)
                .padding(.horizontal, 24)
                .padding(.vertical, 16)
        })
    }
    
    var notGrantedAccessView: some View {
        VStack(alignment: .center, spacing: 10) {
            Image(Constants.settingsIconName)
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
            Text(Constants.accessNotGrantedText)
                .fontWeight(.semibold)
            Button {
                viewModel.openAppSettings()
            } label: {
                Text(Constants.goToSettingsText)
                    .fontWeight(.bold)
                    .foregroundColor(.white)
                    .frame(width: 200, height: 30)
                    .background(
                        LinearGradient.enabledButtonGradient
                    )
                    .clipShape(.capsule)
            }
        }
    }
    
    var contactsList: some View {
            LazyVStack {
                ForEach(viewModel.contacts, id: \.id) { contact in
                    VStack {
                        ContactRow(
                            contact: contact,
                            isSelected: viewModel.selectedContacts.contains(where: { $0.id == contact.id }),
                            searchText: viewModel.searchText
                        ) {
                            viewModel.toggleSelection(for: contact)
                        }
                        Divider()
                    }
                }
        }
        .padding(20)
    }
    
    @ViewBuilder
    var contentView: some View {
        ScrollView {
            if viewModel.isContactsEmpty && ContactsManager.shared.isAccessGranted {
                Text(Constants.noContactsText)
                    .foregroundColor(.black)
                    .padding()
            } else if !ContactsManager.shared.isAccessGranted {
                notGrantedAccessView
            } else {
                contactsList
            }
        }
    }
}

// MARK: - Constants
private extension ContactsListView {
    enum Constants {
        static let goToSettingsText = "Go to settings"
        static let settingsIconName = "mono_settings"
        static let noContactsText = "Sorry, but you don`t have contacts."
        static let accessNotGrantedText = "Access to contacts is not granted."
        static let continueText = "Continue"
        static let contactsText = "Contacts"
    }
}
