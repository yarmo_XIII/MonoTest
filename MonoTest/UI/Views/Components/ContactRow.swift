//
//  ContactRow.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import SwiftUI

// MARK: - Constants
private extension ContactRow {
    enum Constants {
        static let avatarHeight: CGFloat = 40
        static let avatarWidth: CGFloat = 40
        static let checkmarkIconName = "checkmark.circle.fill"
        static let circleIconName = "circle"
    }
}

// MARK: - ContactRow
struct ContactRow: View {
    var contact: Contact
    var isSelected: Bool
    var searchText: String = String()
    var toggleSelection: () -> Void = { }
    var isCheckmarkHidden: Bool = false

    var body: some View {
        HStack {
            avatarView
            contactInfoView
            Spacer()
            if !isCheckmarkHidden {
                Image(systemName: isSelected ? Constants.checkmarkIconName : Constants.circleIconName)
            }
        }
        .onTapGesture {
            toggleSelection()
        }
        .padding(.vertical, 8)
    }
}

// MARK: - Views
private extension ContactRow {
    @ViewBuilder
    var avatarView: some View {
        if let avatar = contact.avatar {
            Image(uiImage: avatar)
                .resizable()
                .frame(width: Constants.avatarWidth, height: Constants.avatarHeight)
                .clipShape(Circle())
        } else {
            Text(contact.initials)
                .frame(width: Constants.avatarWidth, height: Constants.avatarHeight)
                .background(Color.gray)
                .clipShape(Circle())
        }
    }
    
    var contactInfoView: some View {
        VStack(alignment: .leading) {
            highlightedText(for: contact.fullName)
                .font(.headline)
            if let phoneNumber = contact.phoneNumber {
                highlightedText(for: phoneNumber)
                    .font(.subheadline)
                    .foregroundColor(.gray)
            }
        }
    }
}

// MARK: - Private methods
private extension ContactRow {
    func highlightedText(for text: String) -> Text {
        let ranges = text.ranges(of: searchText.lowercased())
        let parts = text.split(by: ranges)
        
        return parts.reduce(Text("")) { result, part in
            if part.lowercased() == searchText.lowercased() {
                return result + Text(part).foregroundColor(.blue)
            } else {
                return result + Text(part)
            }
        }
    }
}
