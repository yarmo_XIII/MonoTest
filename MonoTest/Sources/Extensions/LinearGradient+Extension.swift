//
//  GradientButtonStyle.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import SwiftUI

extension LinearGradient {
    static let enabledButtonGradient = LinearGradient(colors: [.blue, .purple], startPoint: .leading, endPoint: .trailing)
    static let disabledButtonGradient = LinearGradient(colors: [.gray, .white], startPoint: .leading, endPoint: .trailing)
}
