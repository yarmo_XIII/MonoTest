//
//  String+Extension.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import Foundation

extension String {
    var isCyrillicSymbol: Bool {
        guard let scalar = unicodeScalars.first else { return false }
        return CharacterSet(charactersIn: "\u{0400}"..."\u{04FF}").contains(scalar)
    }
    
    func ranges(of substring: String) -> [Range<String.Index>] {
        var ranges: [Range<String.Index>] = []
        var start = self.startIndex
        
        while let range = self[start..<endIndex].range(of: substring, options: .caseInsensitive) {
            ranges.append(range)
            start = range.upperBound
        }
        
        return ranges
    }

    func split(by ranges: [Range<String.Index>]) -> [Substring] {
        var parts: [Substring] = []
        var previousEndIndex = startIndex
        
        for range in ranges {
            parts.append(self[previousEndIndex..<range.lowerBound])
            parts.append(self[range])
            previousEndIndex = range.upperBound
        }
        
        if previousEndIndex < endIndex {
            parts.append(self[previousEndIndex..<endIndex])
        }
        
        return parts
    }
}
