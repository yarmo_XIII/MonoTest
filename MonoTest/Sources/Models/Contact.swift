//
//  Contact.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import UIKit

// MARK: - Contact
struct Contact: Identifiable {
    let id: UUID
    let firstName: String
    let lastName: String
    let phoneNumber: String?
    let avatar: UIImage?

    // MARK: - Helpers
    var initials: String {
        return "\(firstName.prefix(1))\(lastName.prefix(1))"
    }

    var fullName: String {
        return "\(firstName) \(lastName)"
    }
}
