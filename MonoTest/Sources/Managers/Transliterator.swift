//
//  Transliterator.swift
//  MonoTest
//
//  Created by admin on 29.05.2024.
//

import Foundation

// MARK: - Transliterator
final class Transliterator {
    static let shared = Transliterator()
    private var cache: [String: [String]] = [:]
    private let transliterationMap: [String: [String]] = [
            // Ukrainian to English
            "а": ["a"], "б": ["b"], "в": ["v"], "г": ["h"], "ґ": ["g"], "д": ["d"], "е": ["e"], "є": ["ye", "ie", "je", "e"], "ж": ["zh"], "з": ["z"], "и": ["y"], "і": ["i"], "ї": ["yi", "i"], "й": ["y", "j", "i"], "к": ["k"], "л": ["l"], "м": ["m"], "н": ["n"], "о": ["o"], "п": ["p"], "р": ["r"], "с": ["s"], "т": ["t"], "у": ["u"], "ф": ["f"], "х": ["kh", "h"], "ц": ["ts"], "ч": ["ch"], "ш": ["sh"], "щ": ["shch"], "ь": ["'"], "ю": ["yu", "iu", "ju"], "я": ["ya", "ia", "ja"],
            "А": ["A"], "Б": ["B"], "В": ["V"], "Г": ["H"], "Ґ": ["G"], "Д": ["D"], "Е": ["E"], "Є": ["Ye", "Ie", "Je", "E"], "Ж": ["Zh"], "З": ["Z"], "И": ["Y"], "І": ["I"], "Ї": ["Yi", "I"], "Й": ["Y", "J", "I"], "К": ["K"], "Л": ["L"], "М": ["M"], "Н": ["N"], "О": ["O"], "П": ["P"], "Р": ["R"], "С": ["S"], "Т": ["T"], "У": ["U"], "Ф": ["F"], "Х": ["Kh", "H"], "Ц": ["Ts"], "Ч": ["Ch"], "Ш": ["Sh"], "Щ": ["Shch"], "Ю": ["Yu", "Iu", "Ju"], "Я": ["Ya", "Ia", "Ja"],
            // English to Ukrainian
            "a": ["а"], "b": ["б"], "v": ["в"], "h": ["г", "х"], "g": ["ґ"], "d": ["д"], "e": ["е", "є"], "z": ["з"], "y": ["и", "й"], "i": ["і", "ї"], "k": ["к"], "l": ["л"], "m": ["м"], "n": ["н"], "o": ["о"], "p": ["п"], "r": ["р"], "s": ["с"], "t": ["т"], "u": ["у"], "f": ["ф"], "c": ["ц"], "A": ["А"], "B": ["Б"], "V": ["В"], "H": ["Г", "Х"], "G": ["Ґ"], "D": ["Д"], "E": ["Е", "Є"], "Z": ["З"], "Y": ["И", "Й"], "I": ["І", "Ї"], "K": ["К"], "L": ["Л"], "M": ["М"], "N": ["Н"], "O": ["О"], "P": ["П"], "R": ["Р"], "S": ["С"], "T": ["Т"], "U": ["У"], "F": ["Ф"], "C": ["Ц"],
            "kh": ["х"], "ts": ["ц"], "ch": ["ч"], "sh": ["ш"], "shch": ["щ"], "ye": ["є"], "yu": ["ю"], "ya": ["я"], "yi": ["ї"], "yo": ["йо"], "je": ["є"], "ja": ["я"], "ju": ["ю"], "ji": ["ї"]
        ]
    
    func transliterate(_ string: String) -> [String] {
        if let cachedResult = cache[string] {
            return cachedResult
        }
        
        var results: [String] = [""]
        
        var i = 0
        while i < string.count {
            var matched = false
            for length in stride(from: 4, to: 0, by: -1) {
                let end = min(i + length, string.count)
                let substring = String(string[string.index(string.startIndex, offsetBy: i)..<string.index(string.startIndex, offsetBy: end)])
                if let variants = transliterationMap[substring] {
                    var newResults: [String] = []
                    for variant in variants {
                        for result in results {
                            newResults.append(result + variant)
                        }
                    }
                    results = newResults
                    i += length
                    matched = true
                    break
                }
            }
            if !matched {
                for j in 0..<results.count {
                    results[j].append(string[string.index(string.startIndex, offsetBy: i)])
                }
                i += 1
            }
        }
        
        cache[string] = results
        return results
    }
}
