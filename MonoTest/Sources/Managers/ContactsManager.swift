//
//  ContactsManager.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import Contacts
import UIKit

// MARK: - ContactsManager
final class ContactsManager {
    static let shared = ContactsManager()
    private let store = CNContactStore()
    
    // MARK: - Public methods
    func requestAccess(completion: @escaping (Bool) -> Void) {
        store.requestAccess(for: .contacts) { granted, error in
            completion(granted)
        }
    }
    
    func fetchContacts(completion: @escaping ([Contact]) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var contacts: [Contact] = []
            
            let keys = [
                CNContactGivenNameKey,
                CNContactFamilyNameKey,
                CNContactPhoneNumbersKey,
                CNContactImageDataKey
            ] as [CNKeyDescriptor]
            
            let request = CNContactFetchRequest(keysToFetch: keys)
            
            do {
                try self.store.enumerateContacts(with: request) { cnContact, stop in
                    if self.isContactHaveNeededFiels(contact: cnContact) {
                        let phoneNumber = self.cleanPhoneNumber(cnContact.phoneNumbers.first?.value.stringValue)
                        let contact = Contact(
                            id: UUID(),
                            firstName: cnContact.givenName,
                            lastName: cnContact.familyName,
                            phoneNumber: phoneNumber,
                            avatar: cnContact.imageData.flatMap { UIImage(data: $0) }
                        )
                        contacts.append(contact)
                    }
                }
            } catch {
                print("Failed to fetch contacts:", error)
            }
            
            let sortedContacts = self.sortContacts(contacts: contacts)  
            DispatchQueue.main.async {
                completion(sortedContacts)
            }
        }
    }
    
    func sortContacts(contacts: [Contact]) -> [Contact] {
        let sortedContacts = contacts.sorted { (fisrtContact, secondContact) -> Bool in
            let fisrtContactName = fisrtContact.fullName
            let secondContactName = secondContact.fullName
            
            if fisrtContactName.isCyrillicSymbol && !secondContactName.isCyrillicSymbol {
                return true
            } else if !fisrtContactName.isCyrillicSymbol && secondContactName.isCyrillicSymbol {
                return false
            } else {
                return fisrtContactName.compare(secondContactName, locale: Locale(identifier: "uk")) == .orderedAscending
            }
        }
        
        return sortedContacts
    }
    
    var isAccessGranted: Bool {
        let authorizationStatus = CNContactStore.authorizationStatus(for: .contacts)
        
        switch authorizationStatus {
        case .authorized:
            return true
        case .denied, .restricted:
            return false
        case .notDetermined:
            return false
        @unknown default:
            fatalError()
        }
    }
}

// MARK: - Private methods
private extension ContactsManager {
    func isContactHaveNeededFiels(contact: CNContact) -> Bool {
        !contact.givenName.isEmpty || !contact.familyName.isEmpty || contact.phoneNumbers.first?.value.stringValue != nil
    }
    
    func cleanPhoneNumber(_ phoneNumber: String?) -> String? {
        guard let phoneNumber = phoneNumber else { return nil }
        let cleanedPhoneNumber = phoneNumber.replacingOccurrences(of: "[() ]", with: "", options: .regularExpression)
        return cleanedPhoneNumber
    }
}
