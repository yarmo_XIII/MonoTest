//
//  ContactsCoodrinator.swift
//  MonoTest
//
//  Created by admin on 29.05.2024.
//

import SwiftUI

// MARK: - Coordinator
protocol Coordinator: AnyObject {
    func start()
}

// MARK: - ContactsCoordinator
class ContactsCoordinator: Coordinator {
    let navigationController = UINavigationController()
    
    init() {
        navigationController.navigationBar.isHidden = true
    }
    
    func start() {
        let viewModel = ContactsListViewModel()
        let contactsListView = ContactsListView(coordinator: self, viewModel: viewModel)
        navigationController.pushViewController(UIHostingController(rootView: contactsListView), animated: false)
    }

    func showSelectedContacts(selectedContacts: [Contact]) {
        let viewModel = SelectedContactsViewModel(selectedContacts: selectedContacts)
        let selectedContactsView = SelectedContactsView(viewModel: viewModel)
        navigationController.pushViewController(UIHostingController(rootView: selectedContactsView), animated: true)
    }
}
