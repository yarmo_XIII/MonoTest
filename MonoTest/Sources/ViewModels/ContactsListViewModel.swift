//
//  ContactsListViewModel.swift
//  MonoTest
//
//  Created by admin on 28.05.2024.
//

import SwiftUI
import Combine

// MARK: - ContactsViewModel
final class ContactsListViewModel: ObservableObject {
    @Published var contacts = [Contact]()
    @Published var selectedContacts = [Contact]()
    @Published var searchText = String()
    @Published var isContinueButtonEnabled: Bool = false
    
    private var allContacts: [Contact] = []
    private var cancellables = Set<AnyCancellable>()
    
    init() {
        loadData()
        bind()
    }
}

// MARK: - Public methods
extension ContactsListViewModel {
    var isContactsEmpty: Bool {
        allContacts.isEmpty
    }
    
    var filteredContacts: [Contact] {
        guard !searchText.isEmpty else { return allContacts }
        return allContacts.filter { contact in
            contact.fullName.lowercased().contains(searchText.lowercased()) ||
            contact.phoneNumber?.contains(searchText) == true
        }
    }
    
    func toggleSelection(for contact: Contact) {
        if selectedContacts.contains(where: { $0.id == contact.id }) {
            selectedContacts.removeAll { $0.id == contact.id }
        } else {
            selectedContacts.append(contact)
        }
    }
    
    func openAppSettings() {
        if let appSettingsURL = URL(string: UIApplication.openSettingsURLString) {
            UIApplication.shared.open(appSettingsURL, options: [:], completionHandler: nil)
        }
    }
}

// MARK: - Private methods
private extension ContactsListViewModel {
    func loadData() {
        ContactsManager.shared.requestAccess { [weak self] granted in
            guard let self = self, granted else {
                print("Access to contacts was denied.")
                return
            }
            
            self.loadContacts()
        }
    }
    
    func bind() {
        $searchText
            .debounce(for: .milliseconds(300), scheduler: RunLoop.main)
            .sink { [weak self] searchText in
                guard let self = self else { return }
                self.filterContacts(searchText: searchText)
            }
            .store(in: &cancellables)
        
        $selectedContacts
            .receive(on: RunLoop.main)
            .sink { [weak self] selectedContacts in
                guard let self = self else { return }
                self.isContinueButtonEnabled = !selectedContacts.isEmpty
            }
            .store(in: &cancellables)
    }
    
    func loadContacts() {
        ContactsManager.shared.fetchContacts { [weak self] fetchedContacts in
            self?.allContacts = fetchedContacts
            self?.contacts = fetchedContacts
        }
    }
    
    func filterContacts(searchText: String) {
        guard !searchText.isEmpty else {
            contacts = allContacts
            return
        }
        
        let lowercasedSearchText = searchText.lowercased()
        let transliteratedSearchTexts = Transliterator.shared.transliterate(lowercasedSearchText)
        
        contacts = allContacts.filter { contact in
            let fullNameLowercased = contact.fullName.lowercased()
            let transliteratedFullName = Transliterator.shared.transliterate(fullNameLowercased)
            
            for transliteratedSearchText in transliteratedSearchTexts {
                if fullNameLowercased.contains(transliteratedSearchText) ||
                    transliteratedFullName.contains(where: { $0.contains(transliteratedSearchText) }) ||
                    contact.phoneNumber?.contains(transliteratedSearchText) == true {
                    return true
                }
            }
            
            return false
        }
    }
}
