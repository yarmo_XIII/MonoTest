//
//  SelectedContactsViewModel.swift
//  MonoTest
//
//  Created by admin on 29.05.2024.
//

import SwiftUI

// MARK: - SelectedContactsViewModel
final class SelectedContactsViewModel: ObservableObject {
    private var selectedContacts = [Contact]()
    
    init(selectedContacts: [Contact]) {
        self.selectedContacts = selectedContacts
    }
    
    var sortedContacts: [Contact] {
        ContactsManager.shared.sortContacts(contacts: selectedContacts)
    }
}

